CREATE OR REPLACE package body APPS.xx_wf_pkg
is
	-- +===================================================================+
	-- +                      Workflow Development               +
	-- +===================================================================+
	-- |Author: gowthamraam                                                      |
	-- |Initial Build Date: 01-Mar-2020                                     |
	-- |Source File Name: xx_wf_pkg.pls                 |
	-- |                                                                   |
	-- |Object Name:                                                       |
	-- |Description: <Description or Purpose>                              |
	-- |                                                                   |
	-- |Dependencies:  No                                                  |
	-- |                                                                   |
	-- |Usage:  This pacakge is used in  Workflow Development      |
	-- |                                                                   |
	-- |Parameters : <Required Parameters>                                 |
	-- |             <Optional Parameters>                                 |
	-- |            <Return Codes if any>                                  |
	-- |                                                                   |
	-- |                                                                   |
	-- |Modification History:                                              |
	-- |===============                                                    |
	-- |Version     Date          Author   Remarks                         |
	-- |========= ============= ========= =============================    |
	-- |0.1        01-Mar-2020   gowthamraam     Initial draft version           |
	-- +===================================================================+

	/*============================================================================
	Procedure     : start_process 
	Called by     : This procedure will be called from OAF custom page for Workflow Development 
	Description   : This procedure will be called from OAF custom page for Workflow Development
	=============================================================================*/

procedure start_process       (p_req_id     in            number
                              ,p_error_code    out nocopy number
                              ,p_error_msg     out nocopy varchar2
                              )
is
    ln_user_id            number;
    ln_login_id           number;
    lc_item_type          varchar2(8);
    lc_item_key           varchar2(240);
    lc_status             varchar2(100);
    ln_request_option     number;
    p_iteration           number;
    ln_emp_sal_id         number;
    ln_emp_name varchar2(500);
    begin
        p_error_code    := 0;
        p_error_msg     := 'COMPLETE';
        lc_item_type    := 'XX_REQ_AME';
        lc_item_key     := p_req_id; 

      -- launch workflow
        wf_engine.createprocess (
                                     lc_item_type,
                                    lc_item_key,
                                    'REQ_APPROVAL_PROCESS'
                                 );
        wf_engine.setitemattrtext (
                                    lc_item_type,
                                    lc_item_key,
                                    '#FROM_ROLE',
                                    fnd_profile.value ('USERNAME')
                                    );
                                      
        --Setting the Header Id Primary Key
        wf_engine.setitemattrnumber(
                                    lc_item_type,
                                    lc_item_key,  
                                    'REQ_ID', 
                                    p_req_id);
          wf_engine.setitemattrtext (
                                   lc_item_type,
                                   lc_item_key,
                                   'USER_ATTR',
                                    fnd_profile.value('username')                    
                                   );

        --Setting the Employee Name as the Name is Persistent
        --        open c(p_grv_id);
        --        fetch  c into   ln_emp_name;
        --        close c;

        --        wf_engine.setitemattrtext (
        --                                     lc_item_type
        --                                    ,lc_item_key
        --                                    ,'EMP_NAME',
        --                                     ln_emp_name
        --                                    );




        --Setting the Workflow Item Key 
        --        update xxadt_grievances_hdr 
        --        set wf_item_key =  lc_item_key  
        --        where grv_hdr_id=p_grv_id;
        
        wf_engine.startprocess  (
                               lc_item_type
                              ,lc_item_key
                              );

        wf_engine.setitemowner  (
                               lc_item_type
                              ,lc_item_key
                              ,'SYSADMIN'
                              );       
       commit;
    exception
        when others
        then
           p_error_code := sqlcode;
           p_error_msg  := sqlerrm(sqlcode);

    end start_process;


    procedure config_utilities (
                               p_itemtype       in            varchar2
                              ,p_itemkey        in            varchar2
                              ,p_actid          in            number
                              ,p_funcmode       in            varchar2
                              ,x_resultout         out nocopy varchar2
                              )
    is
    lc_error_msg varchar2(2000);
 
     
        LN_REQ_ID                        NUMBER;
        L_ADHOC_REJECT_ROLE_NAME         VARCHAR2 (500);
        L_ADHOC_APPROVE_ROLE_NAME        VARCHAR2 (500);
        L_ADHOC_APPROVE_USER_ROLE_NAME   VARCHAR2 (500);
        L_ADHOC_REJECT_USER_ROLE_NAME    VARCHAR2 (500);
        V_REQUEST_TYPE                   VARCHAR2 (500);
        CHECKROLE                        BOOLEAN := FALSE;
        LN_ITERATION                     NUMBER;
        LN_RFQ_NUMBER                    VARCHAR2(2000);
        LC_FULL_NAME                     VARCHAR2 (2000);
        LC_REQUEST_NUMBER                VARCHAR2 (100);
        LC_TENDER_TITLE                  VARCHAR2 (2000);
        lc_error_adhoc_role             varchar2(2000);
        lc_error_adhoc_USER_role        varchar2(2000);

    BEGIN
        LN_REQ_ID :=
            WF_ENGINE.GETITEMATTRNUMBER (P_ITEMTYPE,
                                         P_ITEMKEY,
                                         'CNTRCT_SME_ID');
                                         
        IF (P_FUNCMODE = 'RUN')
        THEN
            LN_ITERATION :=
                WF_ENGINE.GETITEMATTRNUMBER (P_ITEMTYPE,
                                             P_ITEMKEY,
                                             'WF_ITERATION');

            IF (LN_ITERATION IS NULL)
            THEN
                LN_ITERATION := 1;
                WF_ENGINE.SETITEMATTRNUMBER (P_ITEMTYPE,
                                             P_ITEMKEY,
                                             'WF_ITERATION',
                                             LN_ITERATION);
            ELSE
                LN_ITERATION := LN_ITERATION + 1;
                WF_ENGINE.SETITEMATTRNUMBER (P_ITEMTYPE,
                                             P_ITEMKEY,
                                             'WF_ITERATION',
                                             LN_ITERATION);
            END IF;
        end if;

    exception
    when others
    then
    lc_error_msg  := sqlerrm(sqlcode);
      xx_debug('Inside Exception in config_utilities'||lc_error_msg);
       X_RESULTOUT :=
                   'ERROR: HAS OCCURED IN CONFIG PROCESS'
                || DBMS_UTILITY.FORMAT_ERROR_STACK;
    end;

    procedure get_approvers (
                               p_itemtype       in            varchar2
                              ,p_itemkey        in            varchar2
                              ,p_actid          in            number
                              ,p_funcmode       in            varchar2
                              ,x_resultout         out nocopy varchar2
                              )
    is
        LN_REQ_ID                        NUMBER;
        lc_error_msg varchar2(2000);
        L_NEXT_APPROVERS     AME_UTIL.APPROVERSTABLE2;
        L_NEXT_APPROVER      AME_UTIL.APPROVERRECORD2;
        L_INDEX              AME_UTIL.IDLIST;
        L_IDS                AME_UTIL.STRINGLIST;
        L_CLASS              AME_UTIL.STRINGLIST;
        L_SOURCE             AME_UTIL.LONGSTRINGLIST;
		LCU_NEXT_APPROVER    AME_UTIL.APPROVERRECORD;
		LN_USER_NAME         VARCHAR2 (200);
		L_COMPLETEYNO        VARCHAR2 (100);
		LN_ITERATION         NUMBER;
    begin
    IF (P_FUNCMODE = 'RUN')
        THEN
            LN_REQ_ID :=
                WF_ENGINE.GETITEMATTRNUMBER (P_ITEMTYPE,
                                             P_ITEMKEY,
                                             'CNTRCT_SME_ID');
        xx_debug('Getting the Request ID '||LN_REQ_ID);                     --To Debug the Workflow Process 


            AME_API2.GETNEXTAPPROVERS4 (
                APPLICATIONIDIN                => 50300,                    --Application ID of the Custom AME Transaction Type
                TRANSACTIONTYPEIN              => 'XXREQ_AME',              --Transaction Type Key Name
                TRANSACTIONIDIN                => LN_REQ_ID,
                FLAGAPPROVERSASNOTIFIEDIN      => AME_UTIL.BOOLEANFALSE,
                APPROVALPROCESSCOMPLETEYNOUT   => L_COMPLETEYNO,
                NEXTAPPROVERSOUT               => L_NEXT_APPROVERS);    


            IF L_NEXT_APPROVERS.COUNT > 0
            THEN
                FOR I IN 1 .. L_NEXT_APPROVERS.COUNT
                LOOP
                    xx_debug('Inside  L_NEXT_APPROVERS.COUNT > 0'||LN_REQ_ID);
                    LN_USER_NAME := L_NEXT_APPROVERS (I).NAME;

                    WF_ENGINE.SETITEMATTRTEXT (P_ITEMTYPE,
                                               P_ITEMKEY,
                                               'NEXT_APPROVER_USER_NAME',
                                               LN_USER_NAME);
                                                xx_debug('Inside  L_NEXT_APPROVERS.COUNT > 0 LN_USER_NAME'||LN_USER_NAME);
                    WF_ENGINE.SETITEMATTRTEXT (P_ITEMTYPE,
                                               P_ITEMKEY,
                                               'WF_AME_TRANS_TYPE',
                                               'XXREQ_AME');
                                                xx_debug('Inside  L_NEXT_APPROVERS.COUNT > 0 XXREQ_AME');
                                               
                    LN_ITERATION :=
                        WF_ENGINE.GETITEMATTRNUMBER (P_ITEMTYPE,
                                                     P_ITEMKEY,
                                                     'WF_ITERATION');
                                                     xx_debug('Inside  L_NEXT_APPROVERS.COUNT > 0 WF_ITERATION'||LN_ITERATION);

                END LOOP;
                        xx_debug('Inside  L_NEXT_APPROVERS.COUNT > 0 End Loops');
                            xx_debug('Inside  WF_ENGINE.ENG_COMPLETED || Y');
                X_RESULTOUT := WF_ENGINE.ENG_COMPLETED || ':' || 'Y';
            ELSE
              xx_debug('Inside  WF_ENGINE.ENG_COMPLETED || N');
                X_RESULTOUT := WF_ENGINE.ENG_COMPLETED || ':' || 'N';
            END IF;
    end if;

    exception
    when others
    then
    lc_error_msg  := sqlerrm(sqlcode);
      xx_debug('Inside Exception in get_approvers'||lc_error_msg);
    end;


   procedure insert_history (
                               p_itemtype       in            varchar2
                              ,p_itemkey        in            varchar2
                              ,p_actid          in            number
                              ,p_funcmode       in            varchar2
                              ,x_resultout         out nocopy varchar2
                              )
    is
        lc_error_msg varchar2(2000);
    begin
    if (p_funcmode='RUN')
    then
        null;
    end if;

    exception
    when others
    then
    lc_error_msg  := sqlerrm(sqlcode);
      xx_debug('Inside Exception in insert_history'||lc_error_msg);
    end;
    
    
   procedure update_history (
                               p_itemtype       in            varchar2
                              ,p_itemkey        in            varchar2
                              ,p_actid          in            number
                              ,p_funcmode       in            varchar2
                              ,x_resultout         out nocopy varchar2
                              )
    is
        lc_error_msg varchar2(2000);
    begin
    if (p_funcmode='RUN')
    then
        null;
    end if;

    exception
    when others
    then
    lc_error_msg  := sqlerrm(sqlcode);
      xx_debug('Inside Exception in update_history'||lc_error_msg);
    end;
            
end xx_wf_pkg;
/
