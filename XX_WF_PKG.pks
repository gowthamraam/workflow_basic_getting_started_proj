CREATE OR REPLACE package APPS.xx_wf_pkg

is
 	-- +===================================================================+
	-- +                      Workflow Development               +
	-- +===================================================================+
	-- |Author: gowthamraam                                                      |
	-- |Initial Build Date: 01-Mar-2020                                     |
	-- |Source File Name: xx_wf_pkg.pls                 |
	-- |                                                                   |
	-- |Object Name:                                                       |
	-- |Description: <Description or Purpose>                              |
	-- |                                                                   |
	-- |Dependencies:  No                                                  |
	-- |                                                                   |
	-- |Usage:  This pacakge is used in  Workflow Development      |
	-- |                                                                   |
	-- |Parameters : <Required Parameters>                                 |
	-- |             <Optional Parameters>                                 |
	-- |            <Return Codes if any>                                  |
	-- |                                                                   |
	-- |                                                                   |
	-- |Modification History:                                              |
	-- |===============                                                    |
	-- |Version     Date          Author   Remarks                         |
	-- |========= ============= ========= =============================    |
	-- |0.1        01-Mar-2020   gowthamraam     Initial draft version           |
	-- +===================================================================+

	/*============================================================================
	Procedure     : start_process 
	Called by     : This procedure will be called from OAF custom page for Workflow Development 
	Description   : This procedure will be called from OAF custom page for Workflow Development
	=============================================================================*/
procedure start_process (p_req_id     in            number
                              ,p_error_code    out nocopy number
                              ,p_error_msg     out nocopy varchar2
                              );

procedure config_utilities (
                           p_itemtype       in            varchar2
                          ,p_itemkey        in            varchar2
                          ,p_actid          in            number
                          ,p_funcmode       in            varchar2
                          ,x_resultout         out nocopy varchar2
                          );

procedure get_approvers (
                           p_itemtype       in            varchar2
                          ,p_itemkey        in            varchar2
                          ,p_actid          in            number
                          ,p_funcmode       in            varchar2
                          ,x_resultout         out nocopy varchar2
                          );
                          
procedure insert_history (
                           p_itemtype       in            varchar2
                          ,p_itemkey        in            varchar2
                          ,p_actid          in            number
                          ,p_funcmode       in            varchar2
                          ,x_resultout         out nocopy varchar2
                          );
                          
procedure update_history (
                           p_itemtype       in            varchar2
                          ,p_itemkey        in            varchar2
                          ,p_actid          in            number
                          ,p_funcmode       in            varchar2
                          ,x_resultout         out nocopy varchar2
                          );  
end xx_wf_pkg;
/
