declare
l_error_code  number;
l_error_msg varchar2(300);
begin
xx_wf_pkg.start_process(1, l_error_code, l_error_msg);
dbms_output.put_line('Error Code is : '||l_error_code||' - Error Msg is '||l_error_msg);
end;